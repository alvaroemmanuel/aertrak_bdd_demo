# Aertrak Automated Acceptance Test Demo

## Install
1. Ensure you have RVM with ruby version 2.3 or newer and bundler installed.
2. Install `chromedriver`. In macOS just use homebrew: `brew install chromedriver`
2. Go to project's folder and run `bundle install`.

## Run
1. Execute cucumber: `cucumber`.
