require 'capybara/cucumber'
require 'rspec/expectations'
require 'capybara/poltergeist'
# require 'site_prism'

Capybara.register_driver :chrome do |app|
  Capybara::Selenium::Driver.new(app, browser: :chrome)
end

Capybara.register_driver :poltergeist do |app|
  Capybara::Poltergeist::Driver.new(app, window_size: [1280, 800],
                                         timeout: 120,
                                         js_errors: false,
                                         phantomjs_options: ['--ignore-ssl-errors=yes'])
end

Capybara.run_server = false
# Capybara.default_max_wait_time = 15
# Capybara.default_driver = ENV['driver'].to_sym
Capybara.default_driver = :chrome
