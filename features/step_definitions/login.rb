Given(/^I'm a registered user$/) do
  @email = 'aeris.aertrak+test1@gmail.com'
  @password = 'AertrakTest1'
end

When(/^I login to aertrak$/) do
  visit 'https://aertrak.aerisdev.com'

  within('#kc-form-login') do
    fill_in 'Email', with: @email
    fill_in 'Password', with: @password

    click_on 'Log in'
  end
end

Then(/^I should land to the home page$/) do
  assert_text('Home')
  # save_screenshot('~/Desktop/Demo/login.png')
end
