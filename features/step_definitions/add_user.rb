Given(/^I'm logged in as a fleetadmin$/) do
  @email = 'aeris.aertrak+test1@gmail.com'
  @password = 'AertrakTest1'

  visit 'https://aertrak.aerisdev.com'

  within('#kc-form-login') do
    fill_in 'Email', with: @email
    fill_in 'Password', with: @password

    click_on 'Log in'
  end
end

When(/^I add a new user$/) do
  @email = "alvaro.valle+aertrak.#{Time.now.to_i}@globant.com"
  visit 'https://aertrak.aerisdev.com/#/users'

  within('#users') do
    add_user_button = find('#addButtonUser')
    add_user_button.click
  end

  within('form#form') do
    fill_in 'email', with: @email
    fill_in 'firstname', with: 'Test'
    fill_in 'lastname', with: 'Test'
    click_on 'Save'
  end
end

Then(/^the new user is created$/) do
  within('table.vaadin-grid') do
    assert_text(@email)
    # save_screenshot('~/Desktop/Demo/create_new_user.png')
  end
end

When(/^I try to add a new user with empty email$/) do
  visit 'https://aertrak.aerisdev.com/#/users'

  within('#users') do
    add_user_button = find('#addButtonUser')
    add_user_button.click
  end

  within('form#form') do
    fill_in 'email', with: ''
    fill_in 'firstname', with: 'Test'
    fill_in 'lastname', with: 'Test'
    click_on 'Save'
  end
end

Then(/^the user should't be created$/) do
  within('form#form') do
    email_field_label = find('label', text: 'Email address')
    expect(email_field_label).to be_visible
    # save_screenshot('~/Desktop/Demo/new_user_empty_email.png')
  end
end