Feature: Add user
  New users can be created using the "Add new user" form.
  Email address is mandatory

  @regression @positive
  Scenario: Create a new user
    Given I'm logged in as a fleetadmin
    When I add a new user
    Then the new user is created

  @negative
  Scenario: Attempt to create a new user with empty email
    Given I'm logged in as a fleetadmin
    When I try to add a new user with empty email
    Then the user should't be created
