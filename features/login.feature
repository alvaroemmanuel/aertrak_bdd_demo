Feature: Login
  Registered user should be able to login

  @positive
  Scenario: Login
    Given I'm a registered user
    When I login to aertrak
    Then I should land to the home page
    